#!/usr/bin/bash

PROJECT=project_template
PLATFORM=android

ANDROID_NDK=${HOME}/.opt/android-ndk
ANDROID_SDK=${HOME}/.opt/android-sdk

# ANDROID_ARCH_NAME=arm64
# ANDROID_ABI=arm64-v8a
ANDROID_ARCH_NAME=arm
ANDROID_ABI=armeabi-v7a
ANDROID_TARGET_API=24
ANDROID_NDK_TOOLCHAIN_FILE=${ANDROID_NDK}/build/cmake/android.toolchain.cmake
INSTALL_PREFIX=${ANDROID_NDK}/platforms/android-${ANDROID_TARGET_API}/arch-${ANDROID_ARCH_NAME}/local.${PROJECT}
TOOLCHAIN_NAME=llvm
TARGET_COMPILER="clang"

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

. ${SCRIPTPATH}/../../dependencies # set the dependencies versions

. ${SCRIPTPATH}/boost.build
. ${SCRIPTPATH}/cppunit.build
. ${SCRIPTPATH}/eigen.build
. ${SCRIPTPATH}/flann.build
. ${SCRIPTPATH}/lastools.build
. ${SCRIPTPATH}/nanoflann.build
. ${SCRIPTPATH}/opencv.build
. ${SCRIPTPATH}/pcl.build
. ${SCRIPTPATH}/qt5.build
. ${SCRIPTPATH}/vtk.build
